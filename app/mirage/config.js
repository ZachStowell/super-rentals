export default function() {
  // this.get('/rentals', function(db, request) {
  //
  //   if (request.queryParams.city !== undefined) {
  //     let filteredRentals = rentals.filter(function(i) {
  //       return i.attributes.city.toLowerCase().indexOf(request.queryParams.city.toLowerCase()) !== -1;
  //     });
  //     return {
  //       data: filteredRentals
  //     };
  //   } else {
  //     return {
  //       data: rentals
  //     };
  //   }
  // });
  this.namespace = '/ember-test/api';

  this.get('/rentals', function(db){
    return {
      data: db.rentals.map(attrs => (
        { type: 'rentals', id: attrs.id, attributes: attrs }
      ))
    };
  });
  this.get('/rentals/:id', function(db, req){
    let id = req.params.id;

    return{
      data: {
        type: 'rentals',
        id: id,
        attributes: db.rentals.find(id)
      }
    };
  });
}
